'''Dado un vector de enteros, encuentra el par de elementos adyacentes 
que tiene el producto más grande y devuelve ese producto.'''

# Dejaré un vector por defecto
vector = [3, 6, -2, -5, 7, 3]

pi=[]
for i in range(len(vector)-1):
    pi.append(vector[i]*vector[i+1])
print("el producto mas grande es: "+str(max(pi)))