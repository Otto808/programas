'''Dado un vector de cadenas, devuelva otro vector que 
contenga todas sus cadenas más largas.'''

# Dejaré un vector por defecto
vec=["a","dedo","oro","solo","feo","sol"]

print(vec)
piv=[]
piv.append(vec[0])
for i in range(len(vec)):
    if len(vec[i])==len(piv[0]):
        piv.append(vec[i])
    if len(vec[i])>len(piv[0]):
        piv=[]
        piv.append(vec[i])
print(piv) 