import cv2
import os
import numpy as np
import time
class Entrenamiento:
    def __init__(self):
        self.__labels = []
        self.__facesData = []
        self.__label = 0
        self.__dataPath = './Data'
    def entrenar(self):
        peopleList = os.listdir(self.__dataPath)
        print('Lista de personas: ', peopleList)
        for nameDir in peopleList:
            personPath = self.__dataPath + '/' + nameDir
            for fileName in os.listdir(personPath):
                self.__labels.append(self.__label)
                self.__facesData.append(cv2.imread(personPath+'/'+fileName,0))
            self.__label = self.__label + 1
        face_recognizer = cv2.face.LBPHFaceRecognizer_create()
        print("Entrenando...")
        inicio = time.time()
        face_recognizer.train(self.__facesData, np.array(self.__labels))
        tiempoEntrenamiento = time.time()-inicio
        print("Tiempo de entrenamiento: ", tiempoEntrenamiento)
        face_recognizer.write('modeloLBPHFace.xml')
        print("Modelo almacenado...")