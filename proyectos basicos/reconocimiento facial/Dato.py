import cv2 as cv
import os
import imutils

class Dato:
    def __init__(self,valorVideo):
        self.__video=cv.VideoCapture(valorVideo)
        self.__dataPath = './Data'
        self.__face_recognizer = cv.face.LBPHFaceRecognizer_create()
        self.__cont = 0
    def tomaFoto(self, personName):
        imagePaths=os.listdir(self.__dataPath)
        print('imagePaths=',imagePaths)
        self.__face_recognizer.read('modeloLBPHFace.xml')
        personPath = self.__dataPath + '/'+personName
        if not os.path.exists(personPath):
            print ('Carpeta creada: ',personPath)
            os.makedirs(personPath)

    def personPash(self,personName):
        personPath = self.__dataPath + '/'+personName
        return personPath

    def cargarFaceClass(self):
        faceClassif = cv.CascadeClassifier(cv.data.haarcascades+'haarcascade_frontalface_default.xml')
        return faceClassif

    def reconFacial(self,faceClassif,personPath):
        while True:
            ret, frame = self.__video.read()
            if ret == False:
                break
            frame = imutils.resize(frame, width=320)
            gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            auxFrame =frame.copy()

            faces = faceClassif.detectMultiScale(gray,1.3,5)
            
            for x,y,w,h in faces:
                frame = cv.rectangle(frame, (x,y), (x+w, y+h), (0, 255, 0), 3)
                rostro= auxFrame[y:y + h, x:x +w]
                rostro= cv.resize(rostro, (720,720), interpolation=cv.INTER_CUBIC)
                cv.imwrite(personPath + '/rostro_{}.jpg'.format(self.__cont),rostro)
                self.__cont=self.__cont+1
            
            cv.imshow('frame', frame)

            k= cv.waitKey(1) 

            if k==27 or self.__cont>=500:
                break
        self.__video.release()
        cv.destroyAllWindows()