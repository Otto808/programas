import os
import cv2 
from Arduino import Arduino

class Reconocimiento:
    def __init__(self,valorVideo):
        self.__video=cv2.VideoCapture(valorVideo)
        self.__dataPath = './Data'
        self.__face_recognizer = cv2.face.LBPHFaceRecognizer_create()
        self.__frame = None
        
    def cargarDatos(self):
        imagePaths=os.listdir(self.__dataPath)
        print('imagePaths=',imagePaths)
        self.__face_recognizer.read('modeloLBPHFace.xml')
        
    def cargarFaceClass(self):
        faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')
        return faceClassif

    def reconFacial(self,faceClassif):
        ar=Arduino()
        cont=0
        pivote=None
        imagePaths=os.listdir(self.__dataPath)
        print("\nPresione esc para cerrar la camara\n")
        while True:
            ret,self.__frame = self.__video.read()
            if ret == False: 
                break
            gray = cv2.cvtColor(self.__frame, cv2.COLOR_BGR2GRAY)
            auxFrame = gray.copy()

            faces = faceClassif.detectMultiScale(gray,1.3,5)
            for (x,y,w,h) in faces:
                rostro = auxFrame[y:y+h,x:x+w]
                rostro = cv2.resize(rostro,(150,150),interpolation= cv2.INTER_CUBIC)
                result = self.__face_recognizer.predict(rostro)

                cv2.putText(self.__frame,'{}'.format(result),(x,y-5),1,1.3,(255,255,0),1,cv2.LINE_AA)

                if result[1] < 85:
                    cv2.putText(self.__frame,'{}'.format(imagePaths[result[0]]),(x,y-25),2,1.1,(0,255,0),1,cv2.LINE_AA)
                    cv2.rectangle(self.__frame, (x,y),(x+w,y+h),(0,255,0),2)
                    pivote=1
            
                else:
                    cv2.putText(self.__frame,'Desconocido',(x,y-20),2,0.8,(0,0,255),1,cv2.LINE_AA)
                    cv2.rectangle(self.__frame, (x,y),(x+w,y+h),(0,0,255),2) 
                    pivote=2
            if cont==30:
                #print(cont)
                if pivote ==1:
                    ar.servoAbierto()
                else:
                    ar.servoCerrado() 
                cont=0
            cont=cont+1
            pivote=2
            #print(cont)
            cv2.imshow('frame',self.__frame)
            k = cv2.waitKey(1)
            if k == 27:
                break
        self.__video.release()
        cv2.destroyAllWindows()