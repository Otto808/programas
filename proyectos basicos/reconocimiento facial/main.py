
from Dato import Dato
from Entrenamiento import Entrenamiento
from Reconocimiento import Reconocimiento

def main():
    val = None
    while val != 0:
        print("\n1) introduzca nombre de nuevo usuario \n")
        print("2) actualizar datos \n")
        print("3) activar camara \n")
        print("0) salir de programa \n")
        val=int(input("escoge una opcion: "))
        if val == 1:
            nombre=input('nuevo registro: ')
            registra=Dato(0)
            registra.tomaFoto(nombre)
            personPath=registra.personPash(nombre)
            faceClassif=registra.cargarFaceClass()
            registra.reconFacial(faceClassif,personPath)
            entrena=Entrenamiento()
            entrena.entrenar()
        if val == 2:
            entrena=Entrenamiento()
            entrena.entrenar()
        if val == 3:
            video = Reconocimiento(0)
            video.cargarDatos()
            faceClassif=video.cargarFaceClass()
            video.reconFacial(faceClassif)
    

if __name__ == "__main__":
    main()