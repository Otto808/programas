#include <Servo.h>
 
String pos; //Objeto servo
int e=0; //Variable para guardar el mensaje recibido por serial
Servo servo;
  
void setup()
{
  Serial.begin(9600);  
  servo.attach(9);
}
  
void loop()
{
   if (Serial.available() >0)
   {
     pos = Serial.readString(); //Leemos el serial
     e=pos.toInt();
     servo.write(e);
   }
}
