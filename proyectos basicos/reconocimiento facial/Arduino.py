import serial

class Arduino:
    def __init__(self):
        self.__ser = serial.Serial('/dev/ttyACM1',9600, timeout=10)

    def servoAbierto(self):
        self.__ser.write(str(150).encode())

    def servoCerrado(self):
        self.__ser.write(str(10).encode())


