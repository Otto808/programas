#Amigo ayúdame yo necesito que una matriz ingresada de 3×4 se invierta y sea una de 4×3 :/

import numpy as np

#nos damos valores
valores=np.arange(1,13)
print(valores)

#dimensionamos la matriz a una 3*4
valores.shape=(3,4)
print(valores)

#dimensionamos la matriz a una 4*3
valores.shape=(4,3)
print(valores)