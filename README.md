# programas

## Idea

Quiero hacer una carpeta con repositorios de programas básicos e ir subiendo el nivel gradualmente, también subiré alguno que otro proyecto.

Cualquier error que cometa o me hagan saber del error del video, lo corregiré y lo subo al git

Muchas gracias!!!!!!!

perfil de Tiktok [Audo](https://www.tiktok.com/@audo_808?_t=8Xgib4o66Pm&_r=1)

# comandos
## Comandos para utilizar el repositorio
* Para clonar el repositorio:
```bash
git clone <URL_REPOSITORIO>
```
* Configurar correo y nombre de usuario:
```bash
git config --global user.email "micorreo@gmail.com"
git config --global user.name "usuario"
```
* Subir archivos
    * Cuando tenemos algún cambio en archivos ya existentes o agregamos archivos nuevos o eliminamos archivos, para que estos cambios se vean reflejados en el repositorio, se debe crear un "commit".
    Un commit es un conjunto de cambios.

    * Para crear un commit se ejecutan los siguientes comandos:
    ```bash
    # Agregar los cambios que queremos al commit
    git add <NOMBRE_ARCHIVO>
    git add [NOMBRE_ARCHIVO1 NOMBRE_ARCHIVO2 NOMBRE_ARCHIVO3]
    ```
    * Para ver el estado de nuestro repositorio y saber que archivos son nuevos, o han sido modificados o agregados a un commit:
    ```bash
    git status
    ```
    * Para crear el commit con los archivos que se agregaron:
    ```bash
    git commit -m "Mensaje descriptivo"
    ```
    * Para subir el commit al repositorio en la nube:
    ```bash
    git push origin <NOMBRE_DE_LA_RAMA>
    ```
    * Para crear una rama:
    ```bash
    git branch <NOMBRE_DE_LA_RAMA>
    ```
    * Para cambiar de rama:
    ```bash
    git checkout <NOMBRE_DE_LA_RAMA>
    ```
    * Para actualizar su repositorio local:
    ```bash
    git pull
    ```
