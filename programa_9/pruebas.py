import unittest
from programa_9 import Cadena

'''Algunas personas están de pie en fila en un parque. Hay árboles 
entre ellos que no se pueden mover. Tu tarea es reorganizar a las 
personas por su altura en un orden no descendente sin mover los 
árboles. ¡La gente puede ser muy alta!'''

class TestCadena(unittest.TestCase):

    def test_cadena_1(self):
        ca=Cadena([-1, 150, 190, 170, -1, -1, 160, 180])
        self.assertEqual(ca, [-1, 150, 160, 170, -1, -1, 180, 190])

    def test_cadena_2(self):
        ca=Cadena([4, 2, 9, 11, 2, 16])
        self.assertEqual(ca, [2, 2, 4, 9, 11, 16])
    
    def test_cadena_3(self):
        ca=Cadena([23, 54, -1, 43, 1, -1, -1, 77, -1])
        self.assertEqual(ca, [1, 23, -1, 43, 54, -1, -1, 77, -1])
    
    def test_cadena_4(self):
        ca=Cadena([-1, 6, -1, -1, 5])
        self.assertEqual(ca, [-1, 5, -1, -1, 6])