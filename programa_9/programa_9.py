'''Algunas personas están de pie en fila en un parque. Hay árboles 
entre ellos que no se pueden mover. Tu tarea es reorganizar a las 
personas por su altura en un orden no descendente sin mover los 
árboles. ¡La gente puede ser muy alta!

Si a[i] = -1, entonces la posición está ocupada por un árbol. De lo 
contrario es la altura de una persona de pie en la posición.

Ejemplo:
Para a=[-1, 150, 190, 170, -1, -1, 160, 180], la salida debería ser
solution(a)=[-1, 150, 160, 170, -1, -1, 180, 190].'''

def Cadena(a):
    vec=[]
    j=0
    for x in a:
        if x!=-1:
            vec.append(x)
    vec.sort()
    for i in range(len(a)):
        if a[i]!=-1:
            a[i]=vec[j]
            j+=1
    return a 
