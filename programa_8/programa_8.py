'''Dadas dos cadenas, encuentre el número de 
caracteres comunes entre ellas.'''

def Cadena(cad1, cad2):
    cont=0
    piv=list(cad2)
    for i in cad1:
        if i in piv:
            piv.pop(piv.index(i))
            cont+=1
    return cont

'''Tomaremos uno de los test para visualizar 
el resultado'''
if __name__ == "__main__":
    resp=Cadena("aabcc", "adcaa")
    print(resp)