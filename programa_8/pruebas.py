import unittest
from programa_8 import Cadena

'''Dadas dos cadenas, encuentre el número de 
caracteres comunes entre ellas.'''

class TestCadena(unittest.TestCase):

    def test_cadena_1(self):
        ca = Cadena("aabcc", "adcaa")
        self.assertEqual(ca, 3)

    def test_cadena_2(self):
        ca = Cadena("foco", "perro")
        self.assertEqual(ca, 1)
    
    def test_cadena_3(self):
        ca = Cadena("abca", "xyzbac")
        self.assertEqual(ca, 3)

    def test_cadena_4(self):
        ca = Cadena("uno", "be")
        self.assertEqual(ca, 0)

    def test_cadena_5(self):
        ca = Cadena("cuentos", "calculo")
        self.assertEqual(ca, 3)
